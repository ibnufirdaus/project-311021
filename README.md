# Take Home Project Documentation

## TASK 1,3,4 are in `[OTHERS] tasks` folder

## Movies Microservice

### Description

This codebase is splitted into 4 sections in separate folder

- `cmd` folder, contains executable point for HTTP & GRPC service
- `server` folder, contains service dependency such as domains, usecases, etc. also contains handler function for each
  type of service
- `usecase` folder, contains business logics to process data
- `domain` folder, contains logic to retrieve, store, and modify data

### GET Detail - /movie/{movie id}

Example Request:

```
localhost:8080/movie/tt0458339
```

Example Response:

```json
{
  "Title": "Captain America: The First Avenger",
  "Year": "2011",
  "Rated": "PG-13",
  "Released": "22 Jul 2011",
  "Runtime": "124 min",
  "Genre": "Action, Adventure, Sci-Fi",
  "Director": "Joe Johnston",
  "Writer": "Christopher Markus, Stephen McFeely, Joe Simon",
  "Actors": "Chris Evans, Hugo Weaving, Samuel L. Jackson",
  "Plot": "Steve Rogers, a rejected military soldier, transforms into Captain America after taking a dose of a \"Super-Soldier serum\". But being Captain America comes at a price as he attempts to take down a war monger and a terrorist organizati"
}
```

### GET List - /movies?searchword={search word}&pagination={page}

Example Request:

```
localhost:8080/movies?searchword=avenger&pagination=1
```

Example Response:

```json
[
  {
    "Title": "Captain America: The First Avenger",
    "Year": "2011",
    "imdbID": "tt0458339",
    "Type": "movie",
    "Poster": "https://m.media-amazon.com/images/M/MV5BMTYzOTc2NzU3N15BMl5BanBnXkFtZTcwNjY3MDE3NQ@@._V1_SX300.jpg"
  },
  {
    "Title": "The Toxic Avenger",
    "Year": "1984",
    "imdbID": "tt0090190",
    "Type": "movie",
    "Poster": "https://m.media-amazon.com/images/M/MV5BNzViNmQ5MTYtMmI4Yy00N2Y2LTg4NWUtYWU3MThkMTVjNjk3XkEyXkFqcGdeQXVyMTQxNzMzNDI@._V1_SX300.jpg"
  },
  {
    "Title": "The Toxic Avenger Part II",
    "Year": "1989",
    "imdbID": "tt0098503",
    "Type": "movie",
    "Poster": "https://m.media-amazon.com/images/M/MV5BODhiNTljYTctMmIzZC00ZGVkLTk2NmItN2RjMzY3ZTYyNDczXkEyXkFqcGdeQXVyMTQxNzMzNDI@._V1_SX300.jpg"
  }
  // omitted for simplicity
]
```