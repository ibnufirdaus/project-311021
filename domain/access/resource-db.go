package access

import (
	"context"
	"fmt"
)

var accessLogs []AccessLog

func init() {
	accessLogs = []AccessLog{}
}

func (rscDB DBResource) writeAccessLog(ctx context.Context, log AccessLog) error {
	accessLogs = append(accessLogs, log)

	fmt.Println("Current Log ==========")
	for _, v := range accessLogs {
		fmt.Println(v)
	}
	fmt.Println("======================")

	return nil
}

func (rscDB DBResource) getAccessLogs(ctx context.Context) ([]AccessLog, error) {
	return accessLogs, nil
}
