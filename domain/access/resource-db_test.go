package access

import (
	"context"
	"reflect"
	"testing"
	"time"
)

func TestDBResource_writeAccessLog(t *testing.T) {
	type args struct {
		ctx context.Context
		log AccessLog
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		{
			name: "success",
			args: args{
				ctx: context.Background(),
				log: AccessLog{
					Endpoint:   "GetMovieByParam",
					AccessTime: time.Now(),
				},
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			rscDB := DBResource{}
			if err := rscDB.writeAccessLog(tt.args.ctx, tt.args.log); (err != nil) != tt.wantErr {
				t.Errorf("writeAccessLog() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestDBResource_getAccessLogs(t *testing.T) {

	dummyLog := []AccessLog{{
		Endpoint:   "GetMovieByParam",
		AccessTime: time.Now(),
	}}

	type args struct {
		ctx context.Context
	}
	tests := []struct {
		name    string
		args    args
		want    []AccessLog
		wantErr bool
	}{
		{
			name: "success",
			args: args{
				context.Background(),
			},
			want:    dummyLog,
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			accessLogs = dummyLog
			rscDB := DBResource{}
			got, err := rscDB.getAccessLogs(tt.args.ctx)
			if (err != nil) != tt.wantErr {
				t.Errorf("getAccessLogs() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("getAccessLogs() got = %v, want %v", got, tt.want)
			}
		})
	}
}
