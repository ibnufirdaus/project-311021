package access

import "context"

type DomainItf interface {
	WriteAccessLog(ctx context.Context, log AccessLog) error
	GetAccessLogs(ctx context.Context) ([]AccessLog, error)
}

type DBResourceItf interface {
	writeAccessLog(ctx context.Context, log AccessLog) error
	getAccessLogs(ctx context.Context) ([]AccessLog, error)
}

type Domain struct {
	DB DBResourceItf
}

func InitDomain() Domain {
	return Domain{
		DB: DBResource{},
	}
}

func (d Domain) WriteAccessLog(ctx context.Context, log AccessLog) error {
	return d.DB.writeAccessLog(ctx, log)
}

func (d Domain) GetAccessLogs(ctx context.Context) ([]AccessLog, error) {
	return d.DB.getAccessLogs(ctx)
}
