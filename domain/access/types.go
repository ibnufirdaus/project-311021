package access

import "time"

type AccessLog struct {
	Endpoint   string
	Params     []interface{}
	AccessTime time.Time
}

type DBResource struct{}
