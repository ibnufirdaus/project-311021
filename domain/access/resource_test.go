package access

import (
	"context"
	"errors"
	"github.com/golang/mock/gomock"
	"reflect"
	"testing"
	"time"
)

func TestInitDomain(t *testing.T) {
	tests := []struct {
		name string
		want Domain
	}{
		{
			name: "Default",
			want: Domain{DBResource{}},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := InitDomain(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("InitDomain() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestDomain_WriteAccessLog(t *testing.T) {
	ctl := gomock.NewController(t)
	rscDBMock := NewMockDBResourceItf(ctl)
	ctl.Finish()

	type fields struct {
		DB DBResourceItf
	}
	type args struct {
		ctx context.Context
		log AccessLog
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		mock    func()
		wantErr bool
	}{
		{
			name: "success",
			fields: fields{
				DB: rscDBMock,
			},
			args: args{
				ctx: context.Background(),
				log: AccessLog{
					Endpoint:   "GetMovieByParam",
					AccessTime: time.Now(),
				},
			},
			mock: func() {
				rscDBMock.EXPECT().writeAccessLog(gomock.Any(), gomock.Any()).Return(nil)
			},
			wantErr: false,
		},
		{
			name: "failed writeAccessLog",
			fields: fields{
				DB: rscDBMock,
			},
			args: args{
				ctx: context.Background(),
				log: AccessLog{
					Endpoint:   "GetMovieByParam",
					AccessTime: time.Now(),
				},
			},
			mock: func() {
				rscDBMock.EXPECT().writeAccessLog(gomock.Any(), gomock.Any()).Return(errors.New("error"))
			},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.mock()
			d := Domain{
				DB: tt.fields.DB,
			}
			if err := d.WriteAccessLog(tt.args.ctx, tt.args.log); (err != nil) != tt.wantErr {
				t.Errorf("WriteAccessLog() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestDomain_GetAccessLogs(t *testing.T) {
	ctl := gomock.NewController(t)
	rscDBMock := NewMockDBResourceItf(ctl)
	ctl.Finish()

	type fields struct {
		DB DBResourceItf
	}
	type args struct {
		ctx context.Context
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		mock    func()
		want    []AccessLog
		wantErr bool
	}{
		{
			name: "success",
			fields: fields{
				DB: rscDBMock,
			},
			args: args{
				ctx: context.Background(),
			},
			mock: func() {
				rscDBMock.EXPECT().getAccessLogs(gomock.Any()).Return([]AccessLog{{
					Endpoint:   "GetMovieByParam",
					AccessTime: time.Now(),
				}}, nil)
			},
			want: []AccessLog{{
				Endpoint:   "GetMovieByParam",
				AccessTime: time.Now(),
			}},
			wantErr: false,
		},
		{
			name: "failed getAccessLogs",
			fields: fields{
				DB: rscDBMock,
			},
			args: args{
				ctx: context.Background(),
			},
			mock: func() {
				rscDBMock.EXPECT().getAccessLogs(gomock.Any()).Return([]AccessLog{}, errors.New("error"))
			},
			want:    []AccessLog{},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.mock()
			d := Domain{
				DB: tt.fields.DB,
			}
			got, err := d.GetAccessLogs(tt.args.ctx)
			if (err != nil) != tt.wantErr {
				t.Errorf("GetAccessLogs() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("GetAccessLogs() got = %v, want %v", got, tt.want)
			}
		})
	}
}
