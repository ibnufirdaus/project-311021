package omdb

import (
	"context"
	"fmt"
)

type DomainItf interface {
	GetMovieByParam(ctx context.Context, searchword string, pagination int) ([]Movie, error)
	GetMovieDetail(ctx context.Context, identifier string) (MovieDetail, error)
}

type HTTPResourceItf interface {
	getMovieByParam(ctx context.Context, searchword string, pagination int) (MovieListResponse, error)
	getMovieDetail(ctx context.Context, identifier string) (MovieDetail, error)
}

type Domain struct {
	HTTP HTTPResourceItf
}

func InitDomain(host string, apiKey string) Domain {
	return Domain{
		HTTP: HTTPResource{
			host:   host,
			apiKey: apiKey,
		},
	}
}

func (d Domain) GetMovieByParam(ctx context.Context, searchword string, pagination int) ([]Movie, error) {

	response, err := d.HTTP.getMovieByParam(ctx, searchword, pagination)
	if err != nil {
		fmt.Printf(fmt.Sprintf("[Error][GetMovieByParam][d.HTTP.getMovieByParam][err:%+v]", err))
		return []Movie{}, err
	}

	return response.Search, nil

}

func (d Domain) GetMovieDetail(ctx context.Context, identifier string) (MovieDetail, error) {

	response, err := d.HTTP.getMovieDetail(ctx, identifier)
	if err != nil {
		fmt.Printf(fmt.Sprintf("[Error][GetMovieDetail][d.HTTP.getMovieDetail][err:%+v]", err))
		return MovieDetail{}, err
	}

	return response, nil

}
