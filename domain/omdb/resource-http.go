package omdb

import (
	"context"
	"fmt"
	"io/ioutil"
	"net/http"

	jsoniter "github.com/json-iterator/go"
)

func (rscHttp HTTPResource) getMovieByParam(ctx context.Context, searchword string, pagination int) (MovieListResponse, error) {
	response, err := http.Get(fmt.Sprintf("%s/?apikey=%s&s=%s&page=%d", rscHttp.host, rscHttp.apiKey, searchword, pagination))
	if err != nil {
		fmt.Printf(fmt.Sprintf("[Error][getMovieByParam][http.Get][err:%+v]", err))
		return MovieListResponse{}, err
	}

	responseData, err := ioutil.ReadAll(response.Body)
	if err != nil {
		fmt.Printf(fmt.Sprintf("[Error][getMovieByParam][ioutil.ReadAll][err:%+v]", err))
		return MovieListResponse{}, err
	}

	result := MovieListResponse{}
	err = jsoniter.Unmarshal(responseData, &result)
	if err != nil {
		fmt.Printf(fmt.Sprintf("[Error][getMovieByParam][jsoniter.Unmarshal][err:%+v]", err))
		return MovieListResponse{}, err
	}

	//fmt.Println(response)

	return result, nil
}

func (rscHttp HTTPResource) getMovieDetail(ctx context.Context, identifier string) (MovieDetail, error) {
	response, err := http.Get(fmt.Sprintf("%s/?apikey=%s&i=%s", rscHttp.host, rscHttp.apiKey, identifier))
	if err != nil {
		fmt.Printf(fmt.Sprintf("[Error][getMovieDetail][http.Get][err:%+v]", err))
		return MovieDetail{}, err
	}

	responseData, err := ioutil.ReadAll(response.Body)
	if err != nil {
		fmt.Printf(fmt.Sprintf("[Error][getMovieDetail][ioutil.ReadAll][err:%+v]", err))
		return MovieDetail{}, err
	}

	result := MovieDetail{}
	err = jsoniter.Unmarshal(responseData, &result)
	if err != nil {
		fmt.Printf(fmt.Sprintf("[Error][getMovieDetail][jsoniter.Unmarshal][err:%+v]", err))
		return MovieDetail{}, err
	}

	//fmt.Println(response)

	return result, nil
}
