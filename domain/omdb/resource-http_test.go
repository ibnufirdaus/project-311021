package omdb

import (
	"context"
	"errors"
	jsoniter "github.com/json-iterator/go"
	"io"
	"io/ioutil"
	"reflect"
	"testing"

	"bou.ke/monkey"
	"net/http"
)

func TestHTTPResource_getMovieByParam(t *testing.T) {
	type fields struct {
		host   string
		apiKey string
	}
	type args struct {
		ctx        context.Context
		searchword string
		pagination int
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		mock    func()
		want    MovieListResponse
		wantErr bool
	}{
		{
			name:   "failed http.Get",
			fields: fields{},
			args: args{
				ctx:        context.Background(),
				searchword: "searchword",
				pagination: 1,
			},
			mock: func() {
				monkey.Patch(http.Get, func(url string) (resp *http.Response, err error) {
					return &http.Response{}, errors.New("error")
				})
			},
			want:    MovieListResponse{},
			wantErr: true,
		},
		{
			name:   "failed ioutil.ReadAll",
			fields: fields{},
			args: args{
				ctx:        context.Background(),
				searchword: "searchword",
				pagination: 1,
			},
			mock: func() {
				monkey.Patch(http.Get, func(url string) (resp *http.Response, err error) {
					return &http.Response{}, nil
				})
				monkey.Patch(ioutil.ReadAll, func(r io.Reader) ([]byte, error) {
					return []byte{}, errors.New("error")
				})
			},
			want:    MovieListResponse{},
			wantErr: true,
		},
		{
			name:   "failed jsoniter.Unmarshal",
			fields: fields{},
			args: args{
				ctx:        context.Background(),
				searchword: "searchword",
				pagination: 1,
			},
			mock: func() {
				monkey.Patch(http.Get, func(url string) (resp *http.Response, err error) {
					return &http.Response{}, nil
				})
				monkey.Patch(ioutil.ReadAll, func(r io.Reader) ([]byte, error) {
					return []byte{}, nil
				})
				monkey.Patch(jsoniter.Unmarshal, func(data []byte, v interface{}) error {
					return errors.New("error")
				})
			},
			want:    MovieListResponse{},
			wantErr: true,
		},
		{
			name:   "success",
			fields: fields{},
			args: args{
				ctx:        context.Background(),
				searchword: "searchword",
				pagination: 1,
			},
			mock: func() {
				monkey.Patch(http.Get, func(url string) (resp *http.Response, err error) {
					return &http.Response{}, nil
				})
				monkey.Patch(ioutil.ReadAll, func(r io.Reader) ([]byte, error) {
					return []byte{}, nil
				})
				monkey.Patch(jsoniter.Unmarshal, func(data []byte, v interface{}) error {
					return nil
				})
			},
			want:    MovieListResponse{},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.mock()
			defer monkey.UnpatchAll()
			rscHttp := HTTPResource{
				host:   tt.fields.host,
				apiKey: tt.fields.apiKey,
			}
			got, err := rscHttp.getMovieByParam(tt.args.ctx, tt.args.searchword, tt.args.pagination)
			if (err != nil) != tt.wantErr {
				t.Errorf("getMovieByParam() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("getMovieByParam() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestHTTPResource_getMovieDetail(t *testing.T) {
	type fields struct {
		host   string
		apiKey string
	}
	type args struct {
		ctx        context.Context
		identifier string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		mock    func()
		want    MovieDetail
		wantErr bool
	}{
		{
			name:   "failed http.Get",
			fields: fields{},
			args: args{
				ctx:        context.Background(),
				identifier: "identifier",
			},
			mock: func() {
				monkey.Patch(http.Get, func(url string) (resp *http.Response, err error) {
					return &http.Response{}, errors.New("error")
				})
			},
			want:    MovieDetail{},
			wantErr: true,
		},
		{
			name:   "failed ioutil.ReadAll",
			fields: fields{},
			args: args{
				ctx:        context.Background(),
				identifier: "identifier",
			},
			mock: func() {
				monkey.Patch(http.Get, func(url string) (resp *http.Response, err error) {
					return &http.Response{}, nil
				})
				monkey.Patch(ioutil.ReadAll, func(r io.Reader) ([]byte, error) {
					return []byte{}, errors.New("error")
				})
			},
			want:    MovieDetail{},
			wantErr: true,
		},
		{
			name:   "failed jsoniter.Unmarshal",
			fields: fields{},
			args: args{
				ctx:        context.Background(),
				identifier: "identifier",
			},
			mock: func() {
				monkey.Patch(http.Get, func(url string) (resp *http.Response, err error) {
					return &http.Response{}, nil
				})
				monkey.Patch(ioutil.ReadAll, func(r io.Reader) ([]byte, error) {
					return []byte{}, nil
				})
				monkey.Patch(jsoniter.Unmarshal, func(data []byte, v interface{}) error {
					return errors.New("error")
				})
			},
			want:    MovieDetail{},
			wantErr: true,
		},
		{
			name:   "success",
			fields: fields{},
			args: args{
				ctx:        context.Background(),
				identifier: "identifier",
			},
			mock: func() {
				monkey.Patch(http.Get, func(url string) (resp *http.Response, err error) {
					return &http.Response{}, nil
				})
				monkey.Patch(ioutil.ReadAll, func(r io.Reader) ([]byte, error) {
					return []byte{}, nil
				})
				monkey.Patch(jsoniter.Unmarshal, func(data []byte, v interface{}) error {
					return nil
				})
			},
			want:    MovieDetail{},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		tt.mock()
		defer monkey.UnpatchAll()
		t.Run(tt.name, func(t *testing.T) {
			rscHttp := HTTPResource{
				host:   tt.fields.host,
				apiKey: tt.fields.apiKey,
			}
			got, err := rscHttp.getMovieDetail(tt.args.ctx, tt.args.identifier)
			if (err != nil) != tt.wantErr {
				t.Errorf("getMovieDetail() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("getMovieDetail() got = %v, want %v", got, tt.want)
			}
		})
	}
}
