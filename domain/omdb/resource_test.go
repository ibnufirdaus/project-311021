package omdb

import (
	"context"
	"errors"
	"github.com/golang/mock/gomock"
	"reflect"
	"testing"
)

func TestDomain_GetMovieByParam(t *testing.T) {
	ctl := gomock.NewController(t)
	rscHTTPMock := NewMockHTTPResourceItf(ctl)
	ctl.Finish()

	type fields struct {
		HTTP HTTPResourceItf
	}
	type args struct {
		ctx        context.Context
		searchword string
		pagination int
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		mock    func()
		want    []Movie
		wantErr bool
	}{
		{
			name: "success",
			fields: fields{
				HTTP: rscHTTPMock,
			},
			args: args{
				ctx:        context.Background(),
				searchword: "searchword",
				pagination: 1,
			},
			mock: func() {
				rscHTTPMock.EXPECT().getMovieByParam(gomock.Any(), "searchword", 1).Return(
					MovieListResponse{Search: []Movie{{Title: "Movie 1"}, {Title: "Movie 2"}}}, nil)
			},
			want:    []Movie{{Title: "Movie 1"}, {Title: "Movie 2"}},
			wantErr: false,
		},
		{
			name: "failed getMovieByParam",
			fields: fields{
				HTTP: rscHTTPMock,
			},
			args: args{
				ctx:        context.Background(),
				searchword: "searchword",
				pagination: 1,
			},
			mock: func() {
				rscHTTPMock.EXPECT().getMovieByParam(gomock.Any(), "searchword", 1).Return(
					MovieListResponse{Search: []Movie{}}, errors.New("error"))
			},
			want:    []Movie{},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.mock()
			d := Domain{
				HTTP: tt.fields.HTTP,
			}
			got, err := d.GetMovieByParam(tt.args.ctx, tt.args.searchword, tt.args.pagination)
			if (err != nil) != tt.wantErr {
				t.Errorf("GetMovieByParam() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("GetMovieByParam() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestDomain_GetMovieDetail(t *testing.T) {
	ctl := gomock.NewController(t)
	rscHTTPMock := NewMockHTTPResourceItf(ctl)
	ctl.Finish()

	type fields struct {
		HTTP HTTPResourceItf
	}
	type args struct {
		ctx        context.Context
		identifier string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		mock    func()
		want    MovieDetail
		wantErr bool
	}{
		{
			name: "success",
			fields: fields{
				HTTP: rscHTTPMock,
			},
			args: args{
				ctx:        context.Background(),
				identifier: "identifier",
			},
			mock: func() {
				rscHTTPMock.EXPECT().getMovieDetail(gomock.Any(), "identifier").Return(
					MovieDetail{Title: "Movie 1"}, nil)
			},
			want:    MovieDetail{Title: "Movie 1"},
			wantErr: false,
		},
		{
			name: "failed getMovieDetail",
			fields: fields{
				HTTP: rscHTTPMock,
			},
			args: args{
				ctx:        context.Background(),
				identifier: "identifier",
			},
			mock: func() {
				rscHTTPMock.EXPECT().getMovieDetail(gomock.Any(), "identifier").Return(
					MovieDetail{}, errors.New("error"))
			},
			want:    MovieDetail{},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.mock()
			d := Domain{
				HTTP: tt.fields.HTTP,
			}
			got, err := d.GetMovieDetail(tt.args.ctx, tt.args.identifier)
			if (err != nil) != tt.wantErr {
				t.Errorf("GetMovieDetail() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("GetMovieDetail() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestInitDomain(t *testing.T) {
	type args struct {
		host   string
		apiKey string
	}
	tests := []struct {
		name string
		args args
		want Domain
	}{
		{
			name: "Default",
			args: args{
				host:   "host",
				apiKey: "apiKey",
			},
			want: Domain{HTTPResource{"host", "apiKey"}},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := InitDomain(tt.args.host, tt.args.apiKey); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("InitDomain() = %v, want %v", got, tt.want)
			}
		})
	}
}
