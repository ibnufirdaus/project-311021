// Code generated by MockGen. DO NOT EDIT.
// Source: resource.go

// Package omdb is a generated GoMock package.
package omdb

import (
	context "context"
	reflect "reflect"

	gomock "github.com/golang/mock/gomock"
)

// MockDomainItf is a mock of DomainItf interface.
type MockDomainItf struct {
	ctrl     *gomock.Controller
	recorder *MockDomainItfMockRecorder
}

// MockDomainItfMockRecorder is the mock recorder for MockDomainItf.
type MockDomainItfMockRecorder struct {
	mock *MockDomainItf
}

// NewMockDomainItf creates a new mock instance.
func NewMockDomainItf(ctrl *gomock.Controller) *MockDomainItf {
	mock := &MockDomainItf{ctrl: ctrl}
	mock.recorder = &MockDomainItfMockRecorder{mock}
	return mock
}

// EXPECT returns an object that allows the caller to indicate expected use.
func (m *MockDomainItf) EXPECT() *MockDomainItfMockRecorder {
	return m.recorder
}

// GetMovieByParam mocks base method.
func (m *MockDomainItf) GetMovieByParam(ctx context.Context, searchword string, pagination int) ([]Movie, error) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "GetMovieByParam", ctx, searchword, pagination)
	ret0, _ := ret[0].([]Movie)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// GetMovieByParam indicates an expected call of GetMovieByParam.
func (mr *MockDomainItfMockRecorder) GetMovieByParam(ctx, searchword, pagination interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "GetMovieByParam", reflect.TypeOf((*MockDomainItf)(nil).GetMovieByParam), ctx, searchword, pagination)
}

// GetMovieDetail mocks base method.
func (m *MockDomainItf) GetMovieDetail(ctx context.Context, identifier string) (MovieDetail, error) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "GetMovieDetail", ctx, identifier)
	ret0, _ := ret[0].(MovieDetail)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// GetMovieDetail indicates an expected call of GetMovieDetail.
func (mr *MockDomainItfMockRecorder) GetMovieDetail(ctx, identifier interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "GetMovieDetail", reflect.TypeOf((*MockDomainItf)(nil).GetMovieDetail), ctx, identifier)
}

// MockHTTPResourceItf is a mock of HTTPResourceItf interface.
type MockHTTPResourceItf struct {
	ctrl     *gomock.Controller
	recorder *MockHTTPResourceItfMockRecorder
}

// MockHTTPResourceItfMockRecorder is the mock recorder for MockHTTPResourceItf.
type MockHTTPResourceItfMockRecorder struct {
	mock *MockHTTPResourceItf
}

// NewMockHTTPResourceItf creates a new mock instance.
func NewMockHTTPResourceItf(ctrl *gomock.Controller) *MockHTTPResourceItf {
	mock := &MockHTTPResourceItf{ctrl: ctrl}
	mock.recorder = &MockHTTPResourceItfMockRecorder{mock}
	return mock
}

// EXPECT returns an object that allows the caller to indicate expected use.
func (m *MockHTTPResourceItf) EXPECT() *MockHTTPResourceItfMockRecorder {
	return m.recorder
}

// getMovieByParam mocks base method.
func (m *MockHTTPResourceItf) getMovieByParam(ctx context.Context, searchword string, pagination int) (MovieListResponse, error) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "getMovieByParam", ctx, searchword, pagination)
	ret0, _ := ret[0].(MovieListResponse)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// getMovieByParam indicates an expected call of getMovieByParam.
func (mr *MockHTTPResourceItfMockRecorder) getMovieByParam(ctx, searchword, pagination interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "getMovieByParam", reflect.TypeOf((*MockHTTPResourceItf)(nil).getMovieByParam), ctx, searchword, pagination)
}

// getMovieDetail mocks base method.
func (m *MockHTTPResourceItf) getMovieDetail(ctx context.Context, identifier string) (MovieDetail, error) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "getMovieDetail", ctx, identifier)
	ret0, _ := ret[0].(MovieDetail)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// getMovieDetail indicates an expected call of getMovieDetail.
func (mr *MockHTTPResourceItfMockRecorder) getMovieDetail(ctx, identifier interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "getMovieDetail", reflect.TypeOf((*MockHTTPResourceItf)(nil).getMovieDetail), ctx, identifier)
}
