package http

import (
	"github.com/gorilla/mux"

	"bibit/server/http/movie"
)

func GetRouter() *mux.Router {

	router := mux.NewRouter().StrictSlash(true)

	router.HandleFunc("/ping", movie.PingHandler)
	router.HandleFunc("/movies", movie.GetMoviesHandler).Methods("GET")
	router.HandleFunc("/movie/{identifier}", movie.GetMovieDetailHandler).Methods("GET")

	return router

}
