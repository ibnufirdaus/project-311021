package movie

import (
	"context"
	"github.com/gorilla/mux"
	"net/http"
	"strconv"

	jsoniter "github.com/json-iterator/go"

	"bibit/server"
)

func PingHandler(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	w.Write([]byte(`{"message": "pong"}`))
}

func GetMoviesHandler(w http.ResponseWriter, r *http.Request) {
	ctx := context.Background()

	searchword := r.URL.Query().Get("searchword")
	paginationStr := r.URL.Query().Get("pagination")

	pagination, _ := strconv.Atoi(paginationStr)

	res, err := server.MovieUsecase.GetMovieByParam(ctx, searchword, pagination)
	if err != nil {
		dataBytes, _ := jsoniter.Marshal(err)
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusInternalServerError)
		w.Write(dataBytes)
	}

	dataBytes, _ := jsoniter.Marshal(res)
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	w.Write(dataBytes)
}

func GetMovieDetailHandler(w http.ResponseWriter, r *http.Request) {
	ctx := context.Background()

	identifier := mux.Vars(r)["identifier"]

	res, err := server.MovieUsecase.GetMovieDetail(ctx, identifier)
	if err != nil {
		dataBytes, _ := jsoniter.Marshal(err)
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusInternalServerError)
		w.Write(dataBytes)
	}

	dataBytes, _ := jsoniter.Marshal(res)
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	w.Write(dataBytes)
}
