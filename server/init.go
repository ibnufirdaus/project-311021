package server

import (
	"bibit/domain/access"
	"bibit/domain/omdb"
	"bibit/usecase/movie"
)

var (
	omdbDomain omdb.Domain
	acDomain   access.Domain

	MovieUsecase *movie.Usecase
)

const (
	host   = "http://www.omdbapi.com"
	apiKey = "((OMITTED))"
)

func InitServer() {
	omdbDomain = omdb.InitDomain(host, apiKey)
	acDomain = access.InitDomain()

	MovieUsecase = movie.InitUsecase(omdbDomain, acDomain)
}
