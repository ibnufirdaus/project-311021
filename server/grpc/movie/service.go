package movie

import (
	"bibit/server"
	"context"
	"fmt"

	"bibit/proto/project.com/bibit/proto"
)

type movieServiceServer struct{}

func NewMovieServiceServer() proto.MovieServiceServer {
	return &movieServiceServer{}
}

func (m movieServiceServer) Search(ctx context.Context, request *proto.SearchRequest) (*proto.SearchResponse, error) {
	res, err := server.MovieUsecase.GetMovieByParam(ctx, request.GetSearchword(), int(request.GetPage()))
	if err != nil {
		fmt.Printf("[Error][Search][GetMovieByParam][err:%+v]\n", err)
		return &proto.SearchResponse{}, err
	}

	response := proto.SearchResponse{}
	movies := []*proto.Movie{}
	for _, v := range res {
		movies = append(movies, &proto.Movie{
			Title:  v.Title,
			Year:   v.Year,
			Imdbid: v.ImdbID,
			Type:   v.Type,
			Poster: v.Poster,
		})
	}
	response.Movies = movies

	return &response, nil
}

func (m movieServiceServer) Detail(ctx context.Context, request *proto.DetailRequest) (*proto.DetailResponse, error) {
	res, err := server.MovieUsecase.GetMovieDetail(ctx, request.GetIdentifier())
	if err != nil {
		fmt.Printf("[Error][Detail][GetMovieDetail][err:%+v]\n", err)
		return &proto.DetailResponse{}, err
	}

	response := proto.DetailResponse{
		Moviedetail: &proto.MovieDetail{
			Title:    res.Title,
			Year:     res.Year,
			Rated:    res.Rated,
			Released: res.Released,
			Runtime:  res.Runtime,
			Genre:    res.Genre,
			Director: res.Director,
			Writer:   res.Writer,
			Actors:   res.Actors,
			Plot:     res.Plot,
		},
	}

	return &response, nil
}
