package movie

import (
	"bibit/domain/access"
	"bibit/domain/omdb"
	"context"
	"errors"
	"github.com/golang/mock/gomock"
	"reflect"
	"testing"
)

func TestInitUsecase(t *testing.T) {
	type args struct {
		omdbDomain omdb.DomainItf
		acDomain   access.DomainItf
	}
	tests := []struct {
		name string
		args args
		want *Usecase
	}{
		{
			name: "Default",
			args: args{},
			want: &Usecase{},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := InitUsecase(tt.args.omdbDomain, tt.args.acDomain); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("InitUsecase() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestUsecase_GetMovieByParam(t *testing.T) {
	ctl := gomock.NewController(t)
	omdbDomainMock := omdb.NewMockDomainItf(ctl)
	acDomainMock := access.NewMockDomainItf(ctl)
	ctl.Finish()

	type fields struct {
		omdbDomain omdb.DomainItf
		acDomain   access.DomainItf
	}
	type args struct {
		ctx        context.Context
		searchword string
		pagination int
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		mock    func()
		want    []omdb.Movie
		wantErr bool
	}{
		{
			name: "GetMovieByParam",
			fields: fields{
				omdbDomain: omdbDomainMock,
				acDomain:   acDomainMock,
			},
			args: args{
				ctx:        context.Background(),
				searchword: "searchword",
				pagination: 1,
			},
			mock: func() {
				acDomainMock.EXPECT().WriteAccessLog(gomock.Any(), gomock.Any()).Return(nil)
				omdbDomainMock.EXPECT().GetMovieByParam(gomock.Any(), "searchword", 1).Return(
					[]omdb.Movie{}, errors.New("error"))
			},
			want:    []omdb.Movie{},
			wantErr: true,
		},
		{
			name: "success",
			fields: fields{
				omdbDomain: omdbDomainMock,
				acDomain:   acDomainMock,
			},
			args: args{
				ctx:        context.Background(),
				searchword: "searchword",
				pagination: 1,
			},
			mock: func() {
				acDomainMock.EXPECT().WriteAccessLog(gomock.Any(), gomock.Any()).Return(nil)
				omdbDomainMock.EXPECT().GetMovieByParam(gomock.Any(), "searchword", 1).Return(
					[]omdb.Movie{{Title: "Movie 1"}, {Title: "Movie 2"}}, nil)
			},
			want:    []omdb.Movie{{Title: "Movie 1"}, {Title: "Movie 2"}},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		tt.mock()
		t.Run(tt.name, func(t *testing.T) {
			uc := &Usecase{
				omdbDomain: tt.fields.omdbDomain,
				acDomain:   tt.fields.acDomain,
			}
			got, err := uc.GetMovieByParam(tt.args.ctx, tt.args.searchword, tt.args.pagination)
			if (err != nil) != tt.wantErr {
				t.Errorf("GetMovieByParam() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("GetMovieByParam() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestUsecase_GetMovieDetail(t *testing.T) {
	ctl := gomock.NewController(t)
	omdbDomainMock := omdb.NewMockDomainItf(ctl)
	acDomainMock := access.NewMockDomainItf(ctl)
	ctl.Finish()

	type fields struct {
		omdbDomain omdb.DomainItf
		acDomain   access.DomainItf
	}
	type args struct {
		ctx        context.Context
		identifier string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		mock    func()
		want    omdb.MovieDetail
		wantErr bool
	}{
		{
			name: "GetMovieByParam",
			fields: fields{
				omdbDomain: omdbDomainMock,
				acDomain:   acDomainMock,
			},
			args: args{
				ctx:        context.Background(),
				identifier: "identifier",
			},
			mock: func() {
				acDomainMock.EXPECT().WriteAccessLog(gomock.Any(), gomock.Any()).Return(nil)
				omdbDomainMock.EXPECT().GetMovieDetail(gomock.Any(), "identifier").Return(
					omdb.MovieDetail{}, errors.New("error"))
			},
			want:    omdb.MovieDetail{},
			wantErr: true,
		},
		{
			name: "success",
			fields: fields{
				omdbDomain: omdbDomainMock,
				acDomain:   acDomainMock,
			},
			args: args{
				ctx:        context.Background(),
				identifier: "identifier",
			},
			mock: func() {
				acDomainMock.EXPECT().WriteAccessLog(gomock.Any(), gomock.Any()).Return(nil)
				omdbDomainMock.EXPECT().GetMovieDetail(gomock.Any(), "identifier").Return(
					omdb.MovieDetail{Title: "Movie 1"}, nil)
			},
			want:    omdb.MovieDetail{Title: "Movie 1"},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.mock()
			uc := &Usecase{
				omdbDomain: tt.fields.omdbDomain,
				acDomain:   tt.fields.acDomain,
			}
			got, err := uc.GetMovieDetail(tt.args.ctx, tt.args.identifier)
			if (err != nil) != tt.wantErr {
				t.Errorf("GetMovieDetail() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("GetMovieDetail() got = %v, want %v", got, tt.want)
			}
		})
	}
}
