package movie

import (
	"bibit/domain/access"
	"context"
	"time"

	"bibit/domain/omdb"
)

type Usecase struct {
	omdbDomain omdb.DomainItf
	acDomain   access.DomainItf
}

func InitUsecase(
	omdbDomain omdb.DomainItf,
	acDomain access.DomainItf,
) *Usecase {
	return &Usecase{
		omdbDomain: omdbDomain,
		acDomain:   acDomain,
	}
}

func (uc *Usecase) GetMovieByParam(ctx context.Context, searchword string, pagination int) ([]omdb.Movie, error) {

	go uc.acDomain.WriteAccessLog(ctx, access.AccessLog{
		Endpoint:   "GetMovieByParam",
		Params:     []interface{}{searchword, pagination},
		AccessTime: time.Now(),
	})

	return uc.omdbDomain.GetMovieByParam(ctx, searchword, pagination)
}

func (uc *Usecase) GetMovieDetail(ctx context.Context, identifier string) (omdb.MovieDetail, error) {

	go uc.acDomain.WriteAccessLog(ctx, access.AccessLog{
		Endpoint:   "GetMovieDetail",
		Params:     []interface{}{identifier},
		AccessTime: time.Now(),
	})

	return uc.omdbDomain.GetMovieDetail(ctx, identifier)
}
