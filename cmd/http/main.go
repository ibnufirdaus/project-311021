package main

import (
	"log"
	"net/http"

	"bibit/server"
	handler "bibit/server/http"
)

func main() {
	server.InitServer()

	rtr := handler.GetRouter()

	log.Fatal(http.ListenAndServe(":8080", rtr))
}
