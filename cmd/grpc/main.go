package main

import (
	"bibit/server"
	"bibit/server/grpc"
	"bibit/server/grpc/movie"
	"context"
	"fmt"
)

func main() {
	ctx := context.Background()

	server.InitServer()

	movieService := movie.NewMovieServiceServer()
	
	err := grpc.RunServer(ctx, movieService, "8090")
	if err != nil {
		fmt.Printf("[Error][RunServer][err:%+v]\n", err)
	}
}
