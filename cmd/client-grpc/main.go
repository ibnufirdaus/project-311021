package main

import (
	"bibit/proto/project.com/bibit/proto"
	"context"
	"log"
	"time"

	"google.golang.org/grpc"
)

func main() {
	address := new(string)
	*address = "localhost:8090"

	// Set up a connection to the server.
	conn, err := grpc.Dial(*address, grpc.WithInsecure())
	if err != nil {
		log.Fatalf("did not connect: %v", err)
	}
	defer conn.Close()

	c := proto.NewMovieServiceClient(conn)

	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	// Call Search
	req1 := proto.SearchRequest{
		Searchword: "avenger",
		Page:       1,
	}
	res1, err := c.Search(ctx, &req1)
	if err != nil {
		log.Fatalf("Search failed: %v", err)
	}
	log.Printf("Search result: <%+v>\n\n", res1)

	// Read
	req2 := proto.DetailRequest{
		Identifier: "tt0458339",
	}
	res2, err := c.Detail(ctx, &req2)
	if err != nil {
		log.Fatalf("Detail failed: %v", err)
	}
	log.Printf("Detail result: <%+v>\n\n", res2)
}
