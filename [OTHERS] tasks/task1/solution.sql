SELECT u.ID, u.UserName, up.UserName
FROM USER u
LEFT JOIN USER up
    on u.Parent = up.ID;