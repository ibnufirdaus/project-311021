//package main
package task3

import (
	"strings"
)

func findFirstStringInBracket(str string) string {
	// return empty if str is empty
	if len(str) <= 0 {
		return ""
	}

	// find index of '(' and ')'
	openingBracketIdx := strings.Index(str,"(")
	closingBracketIdx := strings.Index(str, ")")

	// return empty if either is not found
	if openingBracketIdx < 0 || closingBracketIdx < 0 {
		return ""
	}

	// return slice string inside bracket
	return str[openingBracketIdx+1:closingBracketIdx]
}

//func main() {
//	fmt.Println(findFirstStringInBracket("halo(hehe)haha"))
//	// response
//	// hehe
//}

