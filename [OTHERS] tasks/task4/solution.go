package task4
//package main

import (
	"sort"
)

// this function will transform given `word` into its alphabetically sorted version
// e.g "kita" and "kati" will return same result "aikt"
func sortWord(word string) string {
	var res []rune

	for _, v := range word {
		res = append(res, v)
	}

	sort.Slice(res, func(i, j int) bool {
		return res[i] < res[j]
	})
	return string(res)
}

func groupAnagrams(words []string) [][]string {
	var res [][]string

	refMap := map[string][]string{}

	for _, word := range words {
		// compute its group word
		sortedWord := sortWord(word)

		// add empty list if group word is not registered yet
		if _, ok := refMap[sortedWord]; !ok {
			refMap[sortedWord] = []string{}
		}

		// add word to its groups based on group word
		refMap[sortedWord] = append(refMap[sortedWord], word)
	}

	for _, v := range refMap {
		res = append(res, v)
	}

	return res
}

//func main() {
//	fmt.Println(groupAnagrams([]string{"kita", "atik", "tika", "aku", "kia", "makan", "kua"}))
//	// response
//	// [[kita atik tika] [aku kua] [kia] [makan]]
//}